#ifndef RECEIVER_H
#define RECEIVER_H
#include <QByteArray>
#include <QWidget>
#include <QtNetwork/QtNetwork>
QT_BEGIN_NAMESPACE
  class QLabel;
  class QPushButton;
  class QUdpSocket;
  QT_END_NAMESPACE
  class Receiver : public QWidget
  {
      Q_OBJECT

  public:
      Receiver(QWidget *parent = 0);
  float processPendingDatagrams();
public :
QUdpSocket *udpSocket;
  private:

  };

  #endif
