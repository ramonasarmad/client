#ifndef SENDER_H
#define SENDER_H

#include <QWidget>
#include <qlineedit.h>
#include <qtextedit.h>
#include <QLabel>
#include <QtNetwork/QtNetwork>
#include <QtNetwork/qudpsocket.h>
QT_BEGIN_NAMESPACE
class QLabel;
class QPushButton;
class QUdpSocket;
class QLineEdit;
class QTextEdit;
QT_END_NAMESPACE

class Sender : public QWidget
{
    Q_OBJECT

public:
    Sender(QWidget *parent = 0);
    void broadcastDatagram(QByteArray data);
 public:
    QUdpSocket *udpSocket;



};

#endif
