#include <QtWidgets>
#include <QtNetwork/QtNetwork>
 #include "receiver.h"

 Receiver::Receiver(QWidget *parent)
     : QWidget(parent)
 {

     udpSocket = new QUdpSocket(this);
     udpSocket->bind(45456, QUdpSocket::ShareAddress);

     connect(udpSocket, SIGNAL(readyRead()),
             this, SLOT(processPendingDatagrams()));


 }

float Receiver::processPendingDatagrams()
 {
    while(udpSocket->hasPendingDatagrams())
    {
      QByteArray datagram;
      datagram.resize(udpSocket->pendingDatagramSize());
      udpSocket->readDatagram(datagram.data(), datagram.size());
         auto jason_doc = QJsonDocument::fromJson(datagram);
         QJsonObject json_obj=jason_doc.object();
         QVariantMap json_map=json_obj.toVariantMap();
         float average = json_map["aver"].toFloat();
       qDebug()<<average;
         return (average);
     }
}
