#ifndef MAINWINDOW_H
#define MAINWINDOW_H
#include <QJsonObject>
#include <QMainWindow>
#include <QJsonDocument>
#include <QDebug>
#include <QLabel>
#include <QPushButton>
#include <qwidget.h>
#include <QtNetwork/QtNetwork>
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
