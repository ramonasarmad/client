#-------------------------------------------------
#
# Project created by QtCreator 2016-12-10T10:53:42
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = json
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    sender.cpp \
    receiver.cpp

HEADERS  += mainwindow.h \
    sender.h \
    receiver.h

FORMS    += mainwindow.ui
QT += network
