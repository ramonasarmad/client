
#include <QtWidgets>
#include <QtNetwork/qudpsocket.h>
#include "sender.h"
#include <iostream>

Sender::Sender(QWidget *parent)
    : QWidget(parent)
{

    udpSocket = new QUdpSocket(this);

}


void Sender::broadcastDatagram(QByteArray data)
{
  udpSocket->writeDatagram(data.data(),data.size(),
                             QHostAddress::Broadcast, 45454);

}
