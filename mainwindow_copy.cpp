#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>
#include <QLabel>
#include <sender.h>
#include <QJsonObject>
#include <QByteArray>
#include <receiver.h>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_2_clicked()
{
  QString lesson = ui->lineEdit->text();
  QString score = ui->lineEdit_2->text();
  QString ratio = ui->lineEdit_3->text();
  qDebug()<<lesson<<score<<ratio;
  QJsonObject s;
  s["lesson"]=lesson;
  s["ratio"]=ratio;
  s["score"]=score;
  QJsonDocument doc(s);
  QByteArray datagram= doc.toJson();
  Sender object;
  object.broadcastDatagram(datagram);

}

void MainWindow::on_pushButton_clicked()
{
 QJsonObject p;
 p["score"]=116;
 QJsonDocument doc(p);
 QByteArray n =doc.toJson();
 Sender score;
 score.broadcastDatagram(n);

 Receiver aver;
 float ramo;
 ramo = aver.processPendingDatagrams();
 QString result;
 result.setNum(ramo);
 ui->result->setText(result);

}
